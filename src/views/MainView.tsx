import React from "react";
import ToDoList from "../components/ToDoList";

export const MainView = () => (
    <div>
        <ToDoList/>
    </div>
);