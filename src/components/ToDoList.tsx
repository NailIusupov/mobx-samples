import React from "react";
import {observer} from "mobx-react";
import {ToDoListService} from "../services/ToDoList.service";

const toDosService = new ToDoListService();

const ToDoList = () => {
    return (
        <div>
            <div>
                <input onChange={event => toDosService.setInputValue(event.target.value)}/>
                <button onClick={toDosService.addTodo}>Add</button>
            </div>
            <ul>
                {
                    toDosService.toDos.map((item, index) => (
                        <li key={index} onClick={() => toDosService.onItemClick(index)}>{item.name} {item.clicksCount}</li>
                    ))
                }
            </ul>
            <p>{toDosService.computedString}</p>
        </div>
    );
};

export default observer(ToDoList);