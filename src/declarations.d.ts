declare module '*.png';
declare module '*.jpg';
declare module '*.json';
declare module '*.svg';
declare module 'react-adaptive';
declare module 'classnames';
declare module 'react-transition-group';
declare module 'nanoid';
declare module 'mobx-react';
declare function createRef<T>(): RefObject<T>
declare interface RefObject<T> {
    // immutable
    readonly current: T | null
}
declare function forwardRef<T, P = {}>(
    Component: RefForwardingComponent<T, P>
): ComponentType<P & ClassAttributes<T>>