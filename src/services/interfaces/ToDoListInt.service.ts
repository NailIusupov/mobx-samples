import {ToDoItemModel} from "./ToDoItem.model";

export interface ToDoListIntService {
    toDos: Array<ToDoItemModel>;
    addTodo: () => void;
    setInputValue: (value: string) => void;
}