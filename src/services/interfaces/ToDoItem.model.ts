export interface ToDoItemModel {
    name: string;
    clicksCount: number;
}