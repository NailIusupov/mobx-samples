import {action, computed, observable} from "mobx";
import {ToDoListIntService} from "./interfaces/ToDoListInt.service";
import {ToDoItemModel} from "./interfaces/ToDoItem.model";

export class ToDoListService implements ToDoListIntService {

    @observable
    toDos: Array<ToDoItemModel> = [];

    private inputValue: string = "";

    @action
    addTodo = () => {
        this.toDos.push({ name: this.inputValue, clicksCount: 0 });
    }

    @action
    onItemClick = (index: number) => {
        this.toDos[index].clicksCount++;
    }

    @computed
    get computedString(): string {
        return this.toDos.reduce((res, item) => res + item.name, "");
    }

    setInputValue = (value: string) => {
        this.inputValue = value;
    }
}