import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route, Switch} from 'react-router-dom';
import {MainView} from "./views/MainView";
import {SecondView} from "./views/SecondView";

ReactDOM.render (
    <HashRouter>
        <Switch>
            <Route exact path="/" component={MainView}/>
            <Route exact path="/second" component={SecondView}/>
        </Switch>
    </HashRouter>,
    document.getElementById("app")
);